const express = require('express');
const userRoutes = require('./server/user/user.route');
const petsRoutes = require('./server/pet/pet.route');
const breedsRoutes = require('./server/breed/breed.route');
const bloodTypeRoutes = require('./server/bloodType/bloodType.route');
const gender = require('./server/gender/gender.route');
const stage = require('./server/stage/stage.route');
const size = require('./server/size/size.route');
const authRoutes = require('./server/auth/auth.route');
const tutorRoutes = require('./server/tutor/tutor.route');
const personRoutes = require('./server/person/person.route');
const vaccineRoutes = require('./server/vaccine/vaccine.route');
const systemRoutes = require('./server/system/system.route');

const router = express.Router(); // eslint-disable-line new-cap

// TODO: use glob to match *.route files

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK')
);

// mount user routes at /users
router.use('/user', userRoutes);
router.use('/pet', petsRoutes);
router.use('/breed', breedsRoutes);
router.use('/gender', gender);
router.use('/bloodType', bloodTypeRoutes);
router.use('/size', size);
router.use('/stage', stage);
router.use('/person', personRoutes);
router.use('/tutor', tutorRoutes);
router.use('/vaccine', vaccineRoutes);
router.use('/system', systemRoutes);

// mount auth routes at /auth
router.use('/auth', authRoutes);

module.exports = router;
