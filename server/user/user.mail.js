const mailer = require('../helpers/mailer');

const userMail = {
  sendForgotPassword: (user, passCode) => {
    const params = {
      to: user.email + ',topogigiovanni@gmail.com,fernando.bombassaro@gmail.com',
      subject: 'Your code',
      body: `
        Hello ${user.firstname},
        <br>
        Your code is
        <br>
        <strong>${passCode}</strong>
      `
    };

    return mailer.sendMail(params);
  }
};

module.exports = userMail;
