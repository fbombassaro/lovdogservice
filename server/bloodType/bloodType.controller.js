const BloodType = require('./bloodType.model');
const BaseController = require('../base/controller');
const _ = require('lodash');

class BloodTypeController extends BaseController {
  constructor() {
    super('bloodType', BloodType);
    this.attachCreator = true;
  }  
  listFilter(req, res, next) {
  
    req.body.sorters = {
      ...req.body.sorters,
      sort: 'name'
    };
  
    return super.listFilter(req, res, next)
  }
}

module.exports = BloodTypeController;
