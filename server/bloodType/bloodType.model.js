const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../base/modelHookHandler');
/**
 * Schema
 */
const BloodTypeSchema = new mongoose.Schema({
  createdBy: {type: String},
  name: {type: String, required: true}
}, {
    timestamps: true
  });

  BloodTypeSchema.plugin(mongoosePaginate);

modelHookHandler(BloodTypeSchema).withStaticMethods();

module.exports = mongoose.model('BloodType', BloodTypeSchema);
