const Tutor = require('./tutor.model');
const BaseController = require('../base/controller');

class TutorController extends BaseController {
  constructor() {
    super('tutor', Tutor);
    this.attachCreator = true;
  }
}

module.exports = TutorController;
