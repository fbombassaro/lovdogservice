const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../base/modelHookHandler');
/**
 * Schema
 */
const TutorSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  isActive: {
    type: Boolean,
    default: true
  }
}, {
  timestamps: true
});

TutorSchema.plugin(mongoosePaginate);

modelHookHandler(TutorSchema)
  .withStaticMethods();

module.exports = mongoose.model('Tutor', TutorSchema);
