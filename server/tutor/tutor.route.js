const express = require('express');
const validate = require('express-validation');
const paramValidation = require('./tutor.validation');
const Controller = require('./tutor.controller');
const authMiddleware = require('../auth/auth.middleware');

const ctrl = new Controller();
const router = express.Router();

//router.use(authMiddleware.hasAuthorization);

router.route('/')
  .get(ctrl.listFilter.bind(ctrl))
  .post(
    validate(paramValidation.create),
    ctrl.create.bind(ctrl)
  );

router.route('/filter')
  .post(ctrl.listFilter.bind(ctrl));

router.route('/:tutorId')
  .get(ctrl.get.bind(ctrl))
  .put(
    validate(paramValidation.update),
    ctrl.update.bind(ctrl)
  )
  .delete(ctrl.remove.bind(ctrl));

router.param('tutorId', ctrl.load.bind(ctrl));

module.exports = router;
