const Joi = require('joi');

const baseSchema = Joi.object({
  // name: Joi.string().required(),
  // availabilityDateFrom: Joi.date().less(Joi.ref('availabilityDateTo')).required(),
  // availabilityDateTo: Joi.date().required(),
  // recurrenceMode: Joi.string().required(),
  // ruleStartHour: Joi.number().min(0).max(23).required(),
  // ruleStartMin: Joi.number().min(0).max(30).valid([0, 30]).required(),
  // ruleEndHour: Joi.number().min(0).max(23).greater(Joi.ref('ruleStartHour')).required(),
  // ruleEndMin: Joi.number().min(0).max(30).valid([0, 30]).required()
});

module.exports = {
  create: {
    body: baseSchema
  },

  update: {
    body: baseSchema,
    params: {
      tutorId: Joi.string().hex().required()
    }
  }
};
