const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../base/modelHookHandler');
/**
 * Schema
 */
const GenderSchema = new mongoose.Schema({
  createdBy: {type: String},
  name: {type: String, required: true}
}, {
    timestamps: true
  });

  GenderSchema.plugin(mongoosePaginate);

modelHookHandler(GenderSchema).withStaticMethods();

module.exports = mongoose.model('Gender', GenderSchema);
