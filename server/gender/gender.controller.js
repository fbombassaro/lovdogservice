const Gender = require('./gender.model');
const BaseController = require('../base/controller');
const _ = require('lodash');

class GenderController extends BaseController {
  constructor() {
    super('gender', Gender);
    this.attachCreator = true;
  }  
  listFilter(req, res, next) {
  
    req.body.sorters = {
      ...req.body.sorters,
      sort: 'name'
    };
  
    return super.listFilter(req, res, next)
  }
}

module.exports = GenderController;
