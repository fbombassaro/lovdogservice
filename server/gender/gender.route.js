const express = require('express');
const validate = require('express-validation');
const paramValidation = require('./gender.validation');
const Controller = require('./gender.controller');
const authMiddleware = require('../auth/auth.middleware');

const ctrl = new Controller();
const router = express.Router();

router.use(authMiddleware.hasAuthorization);

router.route('/')
  .get(ctrl.listFilter.bind(ctrl))
  .post(
    validate(paramValidation.create),
    ctrl.create.bind(ctrl)
  );

router.route('/:genderId')
  .get(ctrl.get.bind(ctrl))
  .put(
    ctrl.update.bind(ctrl)
  )
  .delete(ctrl.remove.bind(ctrl)
);

router.param('genderId', ctrl.load.bind(ctrl));

module.exports = router;
