const mongoose = require('mongoose');
const { exec } = require('child_process');
const config = require('../../config/config');

const mongoUri = config.mongo.host;

class SystemController {
  cleanDb(rer, res, next) {

    const onConnect = (err) => {
      if (err) {
        console.error(err.stack);
      } else {

        mongoose.connection.db.dropDatabase();
      }
    };

    mongoose.connect(
      mongoUri,
      {
        useNewUrlParser: true,
        useCreateIndex: true
      },
      onConnect
    );

    return res.json({
      ok: true
    });
  }

  installData(req, res, next) {
    exec('node install.js', (err, stdout, stderr) => {
      if (err) {
        next(err);
        return;
      }

      req.json({
        stdout,
        stderr
      });
    });
  }
}

module.exports = SystemController;
