const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../base/modelHookHandler');
/**
 * Schema
 */
const PersonSchema = new mongoose.Schema({
  //id: {type: String}, 
  createdBy: {type: String},
  ownerId: {type: String}, // Sistema coloca o id do usuário logado
  name: {type: String, required: true},
  label: {type: String},
}, {
    timestamps: true
  });

  PersonSchema.plugin(mongoosePaginate);

modelHookHandler(PersonSchema).withStaticMethods();

module.exports = mongoose.model('Person', PersonSchema);
