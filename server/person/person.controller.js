const Person = require('./person.model');
const BaseController = require('../base/controller');
const _ = require('lodash');

class PersonController extends BaseController {
  constructor() {
    super('person', Person);
    this.attachCreator = true;
  }  
  listFilter(req, res, next) {
    const userId = _.get(req, 'user._id', null);
    req.body.filters = {
        ...req.body.filters,
        createdBy: userId
    };
    return super.listFilter(req, res, next)
  }
}

module.exports = PersonController;
