const express = require('express');
const validate = require('express-validation');
const Controller = require('./person.controller');
const authMiddleware = require('../auth/auth.middleware');

const ctrl = new Controller();
const router = express.Router();

router.use(authMiddleware.hasAuthorization);

router.route('/')
  .get(ctrl.listFilter.bind(ctrl))
  .post(ctrl.create.bind(ctrl));

router.route('/filter')
  .post(ctrl.listFilter.bind(ctrl));

router.route('/:personId')
  .get(ctrl.get.bind(ctrl))
  .put(ctrl.update.bind(ctrl))
  .delete(ctrl.remove.bind(ctrl));

router.param('personId', ctrl.load.bind(ctrl));

module.exports = router;
