const Vaccine = require('./vaccine.model');
const BaseController = require('../base/controller');

class BreedController extends BaseController {
  constructor() {
    super('vaccine', Vaccine);
  }
  listFilter(req, res, next) {
  
    req.body.sorters = {
      ...req.body.sorters,
      sort: 'name'
    };
  
    return super.listFilter(req, res, next)
  }
}

module.exports = BreedController;
