const express = require('express');
const Controller = require('./vaccine.controller');
const authMiddleware = require('../auth/auth.middleware');

const ctrl = new Controller();
const router = express.Router();

router.use(authMiddleware.hasAuthorization);

router.route('/')
  .get(ctrl.listFilter.bind(ctrl))
  .post(ctrl.create.bind(ctrl));

router.route('/filter')
  .post(ctrl.listFilter.bind(ctrl));

router.route('/:vaccineId')
  .get(ctrl.get.bind(ctrl))
  .put(ctrl.update.bind(ctrl))
  .delete(ctrl.remove.bind(ctrl));

router.param('vaccineId', ctrl.load.bind(ctrl));

module.exports = router;
