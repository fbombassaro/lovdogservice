const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../base/modelHookHandler');
/**
 * Schema
 */
const VaccineSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  providerId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pet',
    required: false // TODO: deve ser true
  }
}, {
  timestamps: true
});

VaccineSchema.plugin(mongoosePaginate);

modelHookHandler(VaccineSchema).withStaticMethods();

module.exports = mongoose.model('Vaccine', VaccineSchema);
