const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../../base/modelHookHandler');
/**
 * Schema
 */
const PetAddressSchema = new mongoose.Schema({
  petId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pet',
    required: true
  },
  postalCode:{type: String, required: true},
  streetAddress:{type: String, required: true},
  district:{type: String},
  city:{type: String},
  state:{type: String},
}, {
  timestamps: true
});

PetAddressSchema.plugin(mongoosePaginate);

modelHookHandler(PetAddressSchema).withStaticMethods();

module.exports = mongoose.model('Address', PetAddressSchema);
