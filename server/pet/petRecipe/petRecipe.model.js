const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../../base/modelHookHandler');
/**
 * Schema
 */
const PetRecipeSchema = new mongoose.Schema({
  petId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pet',
    required: true
  },
  name:{type: String, required: true},
  pictures:{type: [String]}
}, {
  timestamps: true
});

PetRecipeSchema.plugin(mongoosePaginate);

modelHookHandler(PetRecipeSchema).withStaticMethods();

module.exports = mongoose.model('PetRecipe', PetRecipeSchema);
