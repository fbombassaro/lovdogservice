const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../base/modelHookHandler');

/**
 * Schema
 */
const PetSchema = new mongoose.Schema({
  //id: {type: String},
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  ownerId: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    populate: true
  }], // Sistema coloca o id do usuário logado
  tutor: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Tutor',
    populate: true
  }],
  name: {
    type: String,
    required: true
  },
  bio: {
    type: String
  },
  birthdate: {
    type: String
  },
  weight: {
    type: Number
  },
  height: {
    type: Number
  },
  bloodType: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'BloodType',
    populate: true
  },
  stage: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Stage',
    populate: true
  },
  breed: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Breed',
    populate: true
  },
  gender: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Gender',
    populate: true
  },
  size: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Size',
    populate: true
  },
  cnp: {
    type: String
  }, // Gerado pelo sistema
  rga: {
    type: String
  }, // Pendente de definição
  passport: {
    type: String
  },
  pedigreeId: {
    type: String
  },
  token: {
    type: String
  }, // Gerado pelo sistema
  qrcode: {
    type: String
  }, // Gerado pelo sistema
  age: {
    type: String
  }, // Calculado pelo sistema
  pictures: {
    type: [String]
  }
}, {
  timestamps: true
});

PetSchema.plugin(mongoosePaginate);

modelHookHandler(PetSchema).withStaticMethods();

module.exports = mongoose.model('Pet', PetSchema);
