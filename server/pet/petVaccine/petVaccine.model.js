const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../../base/modelHookHandler');
/**
 * Schema
 */
const PetVaccineSchema = new mongoose.Schema({
  vaccineId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pet',
    required: true
  },
  doses: {
    type: [Date],
    required: false
  },
  petId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pet',
    required: true
  },
  manufacturer:{type: String}
}, {
  timestamps: true
});

PetVaccineSchema.plugin(mongoosePaginate);

modelHookHandler(PetVaccineSchema).withStaticMethods();

module.exports = mongoose.model('PetVaccine', PetVaccineSchema);
