const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../../base/modelHookHandler');
/**
 * Schema
 */
const PetMedicineSchema = new mongoose.Schema({
  petId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pet',
    required: true
  },
  name:{type: String, required: true},
  reminder:{type: String, required: true},
  startTime:{ type: String },
  endTime:{ type: String },
  startDate:{type: Date, required: true},
  endDate:{type: Date, required: true},
  manufacturer:{type: String, required: true}
}, {
  timestamps: true
});

PetMedicineSchema.plugin(mongoosePaginate);

modelHookHandler(PetMedicineSchema).withStaticMethods();

module.exports = mongoose.model('PetMedicine', PetMedicineSchema);
