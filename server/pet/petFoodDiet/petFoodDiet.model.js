const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../../base/modelHookHandler');
/**
 * Schema
 */
const PetFoodDietSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  hour: {
    type: String,
    required: false
  },
  type: {
    type: String,
    required: false
  },
  brand: {
    type: String,
    required: false
  },
  manufacturer: {
    type: String,
    required: false
  },
  prepared: {
    type: String,
    required: false
  },
  petId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pet',
    required: true
  }
}, {
  timestamps: true
});

PetFoodDietSchema.plugin(mongoosePaginate);

modelHookHandler(PetFoodDietSchema).withStaticMethods();

module.exports = mongoose.model('PetFoodDiet', PetFoodDietSchema);
