const _ = require('lodash');
const mongoose = require('mongoose');
const QRCode = require('qrcode');
const Pet = require('./pet.model');
const BaseController = require('../base/controller');

class PetController extends BaseController {
  constructor() {
    super('pet', Pet);
    this.attachCreator = true;
  }

  listFilter(req, res, next) {

    const userId = _.get(req, 'user._id', null);

    req.body.filters = {
      ...req.body.filters,
      createdBy: userId
    };

    return super.listFilter(req, res, next)
  }

  create(req, res, next) {
    let body = _.get(req, 'body', {});
    const userId = _.get(req, 'user._id', null);

    if(this.attachCreator && userId) {
      body = {
        ...body,
        createdBy: mongoose.Types.ObjectId(userId)
      }
    }

    this.model.create(body, (err, object) => {
      if (err) {
        next(err);
      } else {
        QRCode.toDataURL(`{"id":"${object._id.toString()}""}`)
          .then(url => {
            const objectProps = object.toJSON();
            objectProps.qrcode = url;

            delete objectProps._id;
            object.update({...objectProps})
              .then(r => {
                res.json({
                  ...object,
                  ok: true
                });
              })
              .catch(e => next(e));
          })
          .catch(err => next(err));
      }
    });
  }
}

module.exports = PetController;
