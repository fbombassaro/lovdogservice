const Pet = require('../pet.model');
const PetTreatment = require('./petTreatment.model');
const BaseController = require('../../base/controller');
const _ = require('lodash');

class PetTreatmentController extends BaseController {
  constructor() {
    super('petTreatment', PetTreatment);
    // this.attachCreator = true;
  }

  load(req, res, next, id) {
    const {pet} = req;
    if(!pet) {
      return next('Pet not found');
    }

    this.model.find({
      petId: pet._id,
      _id: id
    })
    .then((data) => {
      req[this.namespace] = data;
      return next();
    })
    .catch(e => next(e));
  }

  create(req, res, next) {
    if(!req.pet) {
      return next('Pet not defined');
    }

    req.body = {
      ...req.body,
      petId: req.pet._id
    };

    return super.create(req, res, next)
  }

  listFilter(req, res, next) {

    const userId = _.get(req, 'user._id', null);

    const petId = req.params.petId;

    req.body.filters = {
      ...req.body.filters,
      petId: petId
    };

    return super.listFilter(req, res, next)
  }

  remove(req, res, next) {
    if (!req.pet) {
      return next('Pet not defined');
    }

    req.body = {
      ...req.body,
      petId: req.pet._id
    };

    return super.remove(req, res, next)
  }

}

module.exports = PetTreatmentController;
