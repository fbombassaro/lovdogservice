const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../../base/modelHookHandler');
/**
 * Schema
 */
const PetTreatmentSchema = new mongoose.Schema({
  petId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pet',
    required: true
  },
  name:{type: String, required: true},
  startDate:{type: Date, required: true},
  endDate:{type: Date, required: true},
  specialization:{type: String},
  vetName:{type: String},
  type:{type: String}
}, {
  timestamps: true
});

PetTreatmentSchema.plugin(mongoosePaginate);

modelHookHandler(PetTreatmentSchema).withStaticMethods();

module.exports = mongoose.model('PetTreatment', PetTreatmentSchema);
