const express = require('express');
const validate = require('express-validation');
const paramValidation = require('./pet.validation');
const PetController = require('./pet.controller');
const PetFoodDietController = require('./petFoodDiet/petFoodDiet.controller');
const PetVaccineController = require('./petVaccine/petVaccine.controller');
const PetRecipeController = require('./petRecipe/petRecipe.controller');
const PetMedicineController = require('./petMedicine/petMedicine.controller');
const PetTreatmentController = require('./petTreatment/petTreatment.controller');
const PetAddressController = require('./petAddress/petAddress.controller');
const PetPictureController = require('./petPicture/petPicture.controller');
const authMiddleware = require('../auth/auth.middleware');

const petCtrl = new PetController();
const petVaccineCtrl = new PetVaccineController();
const petFoodDietCtrl = new PetFoodDietController();
const petRecipeCtrl = new PetRecipeController();
const petMedicineCtrl = new PetMedicineController();
const petTreatmentCtrl = new PetTreatmentController();
const petAddressCtrl = new PetAddressController();
const petPictureCtrl = new PetPictureController();

const router = express.Router();
router.use(authMiddleware.hasAuthorization);

router.route('/')
  .get(petCtrl.listFilter.bind(petCtrl))
  .post(
    validate(paramValidation.create),
    petCtrl.create.bind(petCtrl)
  );
router.route('/filter')
  .post(petCtrl.listFilter.bind(petCtrl));

router.route('/:petId')
  .get(petCtrl.get.bind(petCtrl))
  .put(
    petCtrl.update.bind(petCtrl)
  )
  .delete(petCtrl.remove.bind(petCtrl));

router.route('/:petId/foodDiet/')
  .get(petFoodDietCtrl.listFilter.bind(petFoodDietCtrl))
  .post(petFoodDietCtrl.create.bind(petFoodDietCtrl));

router.route('/:petId/foodDiet/:foodDietId')
  .put(petFoodDietCtrl.update.bind(petFoodDietCtrl))
  .delete(petFoodDietCtrl.remove.bind(petFoodDietCtrl));

router.route('/:petId/vaccine/')
  .get(petVaccineCtrl.listFilter.bind(petVaccineCtrl))
  .post(petVaccineCtrl.create.bind(petVaccineCtrl));

router.route('/:petId/vaccine/:vaccineId')
  .put(petVaccineCtrl.update.bind(petVaccineCtrl))
  .delete(petVaccineCtrl.remove.bind(petVaccineCtrl));

// Recipe
router.route('/:petId/petRecipe/')
  .get(petRecipeCtrl.listFilter.bind(petRecipeCtrl))
  .post(petRecipeCtrl.create.bind(petRecipeCtrl));

router.route('/:petId/petRecipe/:petRecipeId')
  .put(petRecipeCtrl.update.bind(petRecipeCtrl))
  .delete(petRecipeCtrl.remove.bind(petRecipeCtrl));

// Medicine
router.route('/:petId/petMedicine/')
  .get(petMedicineCtrl.listFilter.bind(petMedicineCtrl))
  .post(petMedicineCtrl.create.bind(petMedicineCtrl));

router.route('/:petId/petMedicine/:petMedicineId')
  .put(petMedicineCtrl.update.bind(petMedicineCtrl))
  .delete(petMedicineCtrl.remove.bind(petMedicineCtrl));

// Treatment
router.route('/:petId/petTreatment/')
  .get(petTreatmentCtrl.listFilter.bind(petTreatmentCtrl))
  .post(petTreatmentCtrl.create.bind(petTreatmentCtrl));

router.route('/:petId/petTreatment/:petTreatmentId')
  .put(petTreatmentCtrl.update.bind(petTreatmentCtrl))
  .delete(petTreatmentCtrl.remove.bind(petTreatmentCtrl));

 // Address 
router.route('/:petId/petAddress/')
  .get(petAddressCtrl.listFilter.bind(petAddressCtrl))
  .post(petAddressCtrl.create.bind(petAddressCtrl));

router.route('/:petId/petAddress/:petAddressId')
  .put(petAddressCtrl.update.bind(petAddressCtrl))
  .delete(petAddressCtrl.remove.bind(petAddressCtrl));

// Picture  
router.route('/:petId/petPicture/')
  .get(petPictureCtrl.listFilter.bind(petPictureCtrl))
  .post(petPictureCtrl.create.bind(petPictureCtrl));

router.route('/:petId/petPicture/:petPictureId')
  .put(petPictureCtrl.update.bind(petPictureCtrl))
  .delete(petPictureCtrl.remove.bind(petPictureCtrl));

router.param('petId', petCtrl.load.bind(petCtrl));
router.param('foodDietId', petFoodDietCtrl.load.bind(petFoodDietCtrl));
router.param('vaccineId', petVaccineCtrl.load.bind(petVaccineCtrl));
router.param('petRecipeId', petRecipeCtrl.load.bind(petRecipeCtrl));
router.param('petMedicineId', petMedicineCtrl.load.bind(petMedicineCtrl));
router.param('petTreatmentId', petTreatmentCtrl.load.bind(petTreatmentCtrl));
router.param('petAddressId', petAddressCtrl.load.bind(petAddressCtrl));
router.param('petPictureId', petPictureCtrl.load.bind(petPictureCtrl));

module.exports = router;
