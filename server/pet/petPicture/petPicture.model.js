const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../../base/modelHookHandler');
/**
 * Schema
 */
const PetPictureSchema = new mongoose.Schema({
  petId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pet',
    required: true
  },
  path:{type: String, required: true},
}, {
  timestamps: true
});

PetPictureSchema.plugin(mongoosePaginate);

modelHookHandler(PetPictureSchema).withStaticMethods();

module.exports = mongoose.model('PetPicture', PetPictureSchema);
