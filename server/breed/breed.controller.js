const Breed = require('./breed.model');
const BaseController = require('../base/controller');
const _ = require('lodash');

class BreedController extends BaseController {
  constructor() {
    super('breed', Breed);
    this.attachCreator = true;
  }  
  listFilter(req, res, next) {
  
    req.body.sorters = {
      ...req.body.sorters,
      sort: 'name',
      limit: 200
    };
  
    return super.listFilter(req, res, next)
  }
}

module.exports = BreedController;
