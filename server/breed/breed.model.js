const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../base/modelHookHandler');
/**
 * Schema
 */
const BreedSchema = new mongoose.Schema({
  createdBy: {type: String},
  name: {type: String, required: true}
}, {
    timestamps: true
  });

  BreedSchema.plugin(mongoosePaginate);

modelHookHandler(BreedSchema).withStaticMethods();

module.exports = mongoose.model('Breed', BreedSchema);
