const express = require('express');
const validate = require('express-validation');
const paramValidation = require('./breed.validation');
const Controller = require('./breed.controller');
const authMiddleware = require('../auth/auth.middleware');

const ctrl = new Controller();
const router = express.Router();

router.use(authMiddleware.hasAuthorization);

router.route('/')
  .get(ctrl.listFilter.bind(ctrl))
  .post(
    validate(paramValidation.create),
    ctrl.create.bind(ctrl)
);

router.route('/:breedId')
  .get(ctrl.get.bind(ctrl))
  .put(
    ctrl.update.bind(ctrl)
  )
  .delete(ctrl.remove.bind(ctrl)
);

router.param('breedId', ctrl.load.bind(ctrl));

module.exports = router;
