const Stage = require('./stage.model');
const BaseController = require('../base/controller');
const _ = require('lodash');

class StageController extends BaseController {
  constructor() {
    super('stage', Stage);
    this.attachCreator = true;
  }  
  listFilter(req, res, next) {
  
    req.body.sorters = {
      ...req.body.sorters,
      sort: 'name'
    };
  
    return super.listFilter(req, res, next)
  }
}

module.exports = StageController;
