const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../base/modelHookHandler');
/**
 * Schema
 */
const StageSchema = new mongoose.Schema({
  createdBy: {type: String},
  name: {type: String, required: true}
}, {
    timestamps: true
  });

  StageSchema.plugin(mongoosePaginate);

modelHookHandler(StageSchema).withStaticMethods();

module.exports = mongoose.model('Stage', StageSchema);
