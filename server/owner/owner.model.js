const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../base/modelHookHandler');

/**
 * Schema
 */
const OwnerSchema = new mongoose.Schema({
  //id: {type: String},
  name: {type: String, required: true},
}, {
    timestamps: true
  });

PetSchema.plugin(mongoosePaginate);

modelHookHandler(PetSchema).withStaticMethods();

module.exports = mongoose.model('Owner', PetSchema);
