const express = require('express');
const validate = require('express-validation');
const paramValidation = require('./size.validation');
const Controller = require('./size.controller');
const authMiddleware = require('../auth/auth.middleware');

const ctrl = new Controller();
const router = express.Router();

router.use(authMiddleware.hasAuthorization);

router.route('/')
  .get(ctrl.listFilter.bind(ctrl))
  .post(
    validate(paramValidation.create),
    ctrl.create.bind(ctrl)
);

router.route('/:sizeId')
  .get(ctrl.get.bind(ctrl))
  .put(
    ctrl.update.bind(ctrl)
  )
  .delete(ctrl.remove.bind(ctrl)
);

router.param('sizeId', ctrl.load.bind(ctrl));

module.exports = router;
