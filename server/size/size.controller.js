const Size = require('./size.model');
const BaseController = require('../base/controller');
const _ = require('lodash');

class SizeController extends BaseController {
  constructor() {
    super('size', Size);
    this.attachCreator = true;
  }  
  listFilter(req, res, next) {
  
    req.body.sorters = {
      ...req.body.sorters,
      sort: 'name'
    };
  
    return super.listFilter(req, res, next)
  }
}

module.exports = SizeController;
