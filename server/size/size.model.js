const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const modelHookHandler = require('../base/modelHookHandler');
/**
 * Schema
 */
const SizeSchema = new mongoose.Schema({
  createdBy: {type: String},
  name: {type: String, required: true}
}, {
    timestamps: true
  });

  SizeSchema.plugin(mongoosePaginate);

modelHookHandler(SizeSchema).withStaticMethods();

module.exports = mongoose.model('Size', SizeSchema);
