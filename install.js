/*
  Use this script at installation
*/

const mongoose = require('mongoose');
const userModel = require('./server/user/user.model');
const breedModel = require('./server/breed/breed.model');
const stageModel = require('./server/stage/stage.model');
const sizeModel = require('./server/size/size.model');
const vaccineModel = require('./server/vaccine/vaccine.model');
const config = require('./config/config');

const mongoUri = config.mongo.host;

const onConnect = (err) => {
  if (err) {
    console.error(err.stack);
  } else {

    userModel.find({}, (err, users) => {
      if (!err) {
        console.log('users', users);

        addAdminUser();
        addADefaultBreeds();
        addADefaultStages();
        addADefaultSizes();
        addVaccines();

      } else {
        console.log(err);
      }

    });
  }
};

function addAdminUser() {
  // pass encoded: $2a$10$j3UJPC3gTyE36Hxw5VyNyea.EI78zsgbm4RPl0R3GXNIQAbD6K0i.
  userModel.create({
    firstname: 'Admin',
    email: 't@t.com',
    password: '123',
    code: '000-000-000-00',
    isAdmin: true,
    isActive: true
  }, (err, user) => {
    if (!err) {
      console.log('User admin created !');
    } else {
      console.log(err);
    }

    mongoose.connection.close();
  });
}

function addADefaultBreeds() {
  breedModel.insertMany([

    {
      "name": "SRD (sem raça definida)"
    }, {
      "name": "Affenpinscher"
    }, {
      "name": "Afghan Hound"
    }, {
      "name": "Airedale Terrier"
    }, {
      "name": "Akita"
    }, {
      "name": "Akita Americano"
    }, {
      "name": "American Pit Bull Terrier"
    }, {
      "name": "American Staffordshire Terrier"
    }, {
      "name": "Australian Shepherd"
    }, {
      "name": "Basenji"
    }, {
      "name": "Basset Fulvo da Bretanha"
    }, {
      "name": "Basset Hound"
    }, {
      "name": "Beagle"
    }, {
      "name": "Beagle-Harrier"
    }, {
      "name": "Bearded Collie"
    }, {
      "name": "Bedlington Terrier"
    }, {
      "name": "Bernese Mountain Dog"
    }, {
      "name": "Bichon Bolonhês"
    }, {
      "name": "Bichon Frisé"
    }, {
      "name": "Bichon Havanês"
    }, {
      "name": "Boerboel"
    }, {
      "name": "Boiadeiro de Entlebuch"
    }, {
      "name": "Border Collie"
    }, {
      "name": "Border Terrier"
    }, {
      "name": "Borzoi"
    }, {
      "name": "Boston Terrier"
    }, {
      "name": "Bouvier de Flandres"
    }, {
      "name": "Boxer"
    }, {
      "name": "Braco Alemão Pelo Curto"
    }, {
      "name": "Braco Alemão Pelo Duro"
    }, {
      "name": "Braco Italiano"
    }, {
      "name": "Buldogue Americano"
    }, {
      "name": "Buldogue Francês"
    }, {
      "name": "Buldogue Inglês"
    }, {
      "name": "Bull Terrier"
    }, {
      "name": "Bullmastiff"
    }, {
      "name": "Cairn Terrier"
    }, {
      "name": "Cane Corso"
    }, {
      "name": "Cão de Crista Chinês"
    }, {
      "name": "Cão de Santo Humberto"
    }, {
      "name": "Cão D’água Espanhol"
    }, {
      "name": "Cão D’água Português"
    }, {
      "name": "Cão Lobo Checoslovaco"
    }, {
      "name": "Cão Pelado Mexicano"
    }, {
      "name": "Cão Pelado Peruano"
    }, {
      "name": "Cavalier King Charles Spaniel"
    }, {
      "name": "Cesky Terrier"
    }, {
      "name": "Chesapeake Bay Retriever"
    }, {
      "name": "Chihuahua"
    }, {
      "name": "Chin"
    }, {
      "name": "Chow-chow Pelo Curto"
    }, {
      "name": "Chow-chow Pelo Longo"
    }, {
      "name": "Cirneco do Etna"
    }, {
      "name": "Clumber Spaniel"
    }, {
      "name": "Cocker Spaniel Americano"
    }, {
      "name": "Cocker Spaniel Inglês"
    }, {
      "name": "Collie pelo longo"
    }, {
      "name": "Coton de Tulear"
    }, {
      "name": "Dachshund Teckel - Pelo Curto"
    }, {
      "name": "Dálmata"
    }, {
      "name": "Dandie Dinmont Terrier"
    }, {
      "name": "Dobermann"
    }, {
      "name": "Dogo Argentino"
    }, {
      "name": "Dogo Canário"
    }, {
      "name": "Dogue Alemão"
    }, {
      "name": "Dogue de Bordeaux"
    }, {
      "name": "Elkhound Norueguês Cinza"
    }, {
      "name": "Fila Brasileiro"
    }, {
      "name": "Flat-Coated Retriever"
    }, {
      "name": "Fox Terrier Pelo Duro"
    }, {
      "name": "Fox Terrier Pelo Liso"
    }, {
      "name": "Foxhound Inglês"
    }, {
      "name": "Galgo Espanhol"
    }, {
      "name": "Golden Retriever"
    }, {
      "name": "Grande Münsterländer"
    }, {
      "name": "Greyhound"
    }, {
      "name": "Griffon Belga"
    }, {
      "name": "Griffon de Bruxelas"
    }, {
      "name": "Husky Siberiano"
    }, {
      "name": "Ibizan Hound"
    }, {
      "name": "Irish Soft Coated Wheaten Terrier"
    }, {
      "name": "Irish Wolfhound"
    }, {
      "name": "Jack Russell Terrier"
    }, {
      "name": "Kerry Blue Terrier"
    }, {
      "name": "Komondor"
    }, {
      "name": "Kuvasz"
    }, {
      "name": "Labrador Retriever"
    }, {
      "name": "Lagotto Romagnolo"
    }, {
      "name": "Lakeland Terrier"
    }, {
      "name": "Leonberger"
    }, {
      "name": "Lhasa Apso"
    }, {
      "name": "Malamute do Alasca"
    }, {
      "name": "Maltês"
    }, {
      "name": "Mastiff"
    }, {
      "name": "Mastim Espanhol"
    }, {
      "name": "Mastino Napoletano"
    }, {
      "name": "Mudi"
    }, {
      "name": "Nordic Spitz"
    }, {
      "name": "Norfolk Terrier"
    }, {
      "name": "Norwich Terrier"
    }, {
      "name": "Old English Sheepdog"
    }, {
      "name": "Papillon"
    }, {
      "name": "Parson Russell Terrier"
    }, {
      "name": "Pastor Alemão"
    }, {
      "name": "Pastor Beauceron"
    }, {
      "name": "Pastor Belga"
    }, {
      "name": "Pastor Bergamasco"
    }, {
      "name": "Pastor Branco Suíço"
    }, {
      "name": "Pastor Briard"
    }, {
      "name": "Pastor da Ásia Central"
    }, {
      "name": "Pastor de Shetland"
    }, {
      "name": "Pastor dos Pirineus"
    }, {
      "name": "Pastor Maremano Abruzês"
    }, {
      "name": "Pastor Polonês"
    }, {
      "name": "Pastor Polonês da Planície"
    }, {
      "name": "Pequeno Basset Griffon da Vendéia"
    }, {
      "name": "Pequeno Cão Leão"
    }, {
      "name": "Pequeno Lebrel Italiano"
    }, {
      "name": "Pequinês"
    }, {
      "name": "Perdigueiro Português"
    }, {
      "name": "Petit Brabançon"
    }, {
      "name": "Pharaoh Hound"
    }, {
      "name": "Pinscher Miniatura"
    }, {
      "name": "Podengo Canário"
    }, {
      "name": "Podengo Português"
    }, {
      "name": "Pointer Inglês"
    }, {
      "name": "Poodle Anão"
    }, {
      "name": "Poodle Médio"
    }, {
      "name": "Poodle Standard"
    }, {
      "name": "Poodle Toy"
    }, {
      "name": "Pug"
    }, {
      "name": "Puli"
    }, {
      "name": "Pumi"
    }, {
      "name": "Rhodesian Ridgeback"
    }, {
      "name": "Rottweiler"
    }, {
      "name": "Saluki"
    }, {
      "name": "Samoieda"
    }, {
      "name": "São Bernardo"
    }, {
      "name": "Schipperke"
    }, {
      "name": "Schnauzer"
    }, {
      "name": "Schnauzer Gigante"
    }, {
      "name": "Schnauzer Miniatura"
    }, {
      "name": "Scottish Terrier"
    }, {
      "name": "Sealyham Terrier"
    }, {
      "name": "Setter Gordon"
    }, {
      "name": "Setter Inglês"
    }, {
      "name": "Setter Irlandês Vermelho"
    }, {
      "name": "Setter Irlandês Vermelho e Branco"
    }, {
      "name": "Shar-pei"
    }, {
      "name": "Shiba"
    }, {
      "name": "Shih-Tzu"
    }, {
      "name": "Silky Terrier Australiano"
    }, {
      "name": "Skye Terrier"
    }, {
      "name": "Smoushond Holandês"
    }, {
      "name": "Spaniel Bretão"
    }, {
      "name": "Spinone Italiano"
    }, {
      "name": "Spitz Alemão Anão"
    }, {
      "name": "Spitz Finlandês"
    }, {
      "name": "Spitz Japonês Anão"
    }, {
      "name": "Springer Spaniel Inglês"
    }, {
      "name": "Stabyhoun"
    }, {
      "name": "Staffordshire Bull Terrier"
    }, {
      "name": "Terra Nova"
    }, {
      "name": "Terrier Alemão de caça Jagd"
    }, {
      "name": "Terrier Brasileiro"
    }, {
      "name": "Terrier Irlandês de Glen do Imaal"
    }, {
      "name": "Terrier Preto da Rússia"
    }, {
      "name": "Tibetan Terrier"
    }, {
      "name": "Tosa Inu"
    }, {
      "name": "Vira-Latas"
    }, {
      "name": "Vizsla"
    }, {
      "name": "Volpino Italiano"
    }, {
      "name": "Weimaraner"
    }, {
      "name": "Welsh Corgi Cardigan"
    }, {
      "name": "Welsh Corgi Pembroke"
    }, {
      "name": "Welsh Springer Spaniel"
    }, {
      "name": "Welsh Terrier"
    }, {
      "name": "West Highland White Terrier"
    }, {
      "name": "Whippet"
    }, {
      "name": "Yorkshire Terrier"
    }
  ], (err, user) => {
    if (!err) {
      console.log('Breeds created !');
    } else {
      console.log(err);
    }

    mongoose.connection.close();
  });
}

function addADefaultStages() {
  stageModel.insertMany([{
    "name": "Filhote"
  }, {
    "name": "Jovem"
  }, {
    "name": "Adulto"
  }, {
    "name": "Sênior"
  }], (err, user) => {
    if (!err) {
      console.log('Stages created !');
    } else {
      console.log(err);
    }

    mongoose.connection.close();
  });
}

function addADefaultSizes() {
  sizeModel.insertMany([{
    "name": "Mini"
  }, {
    "name": "Pequeno"
  }, {
    "name": "Médio"
  }, {
    "name": "Grande"
  }, {
    "name": "Gigante"
  }], (err, user) => {
    if (!err) {
      console.log('Sizes created !');
    } else {
      console.log(err);
    }

    mongoose.connection.close();
  });
}

function addVaccines() {
  vaccineModel.insertMany([{
    name: 'Vacina tipo 1'
  }, {
    name: 'Vacina tipo 2'
  }], (err, user) => {
    if (!err) {
      console.log('Vaccines created !');
    } else {
      console.log(err);
    }

    mongoose.connection.close();
  });
}

mongoose.connect(
  mongoUri, {
    useNewUrlParser: true,
    useCreateIndex: true
  },
  onConnect
);
